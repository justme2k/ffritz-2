# requires: REPODIR, DLDIR
#
# Provides:
# SOC		puma6 or puma7
# FWVER		firmware version aa.ii
# FWNUM 	firmware version aaii
# FWMAJ 	major version
# FWMIN 	minor version
# BETA  	beta version number
# MODEL 	6490, 6590, 6591, 6660
# BR_VERSION	BR version suffix, plus glibc version suffix if > 2.28
# BR_VERSION_BASE BR version without glibc suffix
#
#
# 
include $(REPODIR)/conf.mk.dfl
ifneq ($(NO_USER_CONF),1)
-include $(REPODIR)/conf.mk
endif

# Determine image type
#
ITYPE=$(shell echo $(URL) | sed -e 's/.*\.//')
FNAME=$(shell basename $(URL))
DLIMAGE=$(DLDIR)/$(FNAME)

ifeq ($(ITYPE),zip)
# Fetch and extract labor zip
INAME=$(shell mkdir -p $(DLDIR); if [ ! -f $(DLIMAGE) ]; then wget -O $(DLIMAGE) $(URL); fi; unzip -l $(DLIMAGE) | grep image | head -1 | sed -e 's/.*\(FRITZ.*.image\)/\1/')
ifeq ($(INAME),)
error $(URL) is not a valid firmware zip
endif
else
INAME=$(FNAME)
endif

FWVER=$(shell echo $(INAME) | sed -e 's/.*\([0-9]*.\.[0-9]*\).*\.image/\1/')
BETA=$(shell echo $(INAME) | sed -e 's/.*-\([0-9]*\)-Lab.*/-\1/' | grep -v image)
MODEL=$(shell echo $(INAME) | sed -e 's/.*_\(....\)_Cable.*/\1/')
FWNUM=$(subst .,,$(FWVER))


ifeq ($(MODEL),)
$(error Could not determine device model?))
endif

ifeq ($(FWVER),)
$(error Could not determine firmware version ($(INAME) missing?))
endif

FWMAJ=$(shell echo $(FWVER) | sed -e 's/\..*//')
FWMIN=$(shell echo $(FWVER) | sed -e 's/.*\.//')

# $(warning [$(FWVER)] [$(BETA)] [$(MODEL)] [$(FWNUM)] [$(FWMAJ)] [$(FWMIN)])

ifeq ($(MODEL),6591)
SOC=puma7
endif
ifeq ($(MODEL),6660)
SOC=puma7
endif

ifeq ($(MODEL),6490)
SOC=puma6
endif
ifeq ($(MODEL),6590)
SOC=puma6
endif

ifeq ($(SOC),)
$(error unsupported model $(MODEL))
endif

ifeq ($(shell test $(FWMAJ) -lt 7),0)
$(error sorry, firmware too old, try the fritzos6 branch)
endif

ifeq ($(SOC),puma7)
 ifeq ($(BR_VERSION),)
  ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -lt 19 ; echo $$?),0)
   BR_VERSION_BASE=-2019.05
  else # >= 7.19
   BR_VERSION_BASE=-2022.02
  endif
 else
  BR_VERSION_BASE:=$(BR_VERSION)
 endif

# glibc version depends on firmware
# Beginning with firmware 7.39, BR_VERSION gets a _glibc suffix.
# Before that, no suffix and assume 2.28
#
 ifeq ($(GLIBC),)
  ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -lt 39 ; echo $$?),0)
   GLIBC=libc-2.28.so
   GLIBC_SUFFIX:=
  else
   GLIBC=libc-2.30.so
   GLIBC_SUFFIX=_glibc230
  endif
 endif # GLIBC

 BR_VERSION:=$(BR_VERSION_BASE)$(GLIBC_SUFFIX)

 ifneq ($(BR_VERSION_BASE),-2019.05)
  ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -lt 19 ; echo $$?),0)
   $(error This toolchain will likely not work with firmware < 7.19)
  endif
  ifeq ($(PARALLEL),)
   PARALLEL=$(shell cat /proc/cpuinfo | grep processor | wc -l)
  endif # PARALLEL
 endif # !2019.05

else #puma6
 BR_VERSION:=-2019.05-p6
endif

# default toolchain
#
ifeq ($(BR_VERSION),)
BR_VERSION  = -2019.05
endif

ARM_IMG_VERSION=$(shell cat $(REPODIR)/packages/arm/ffritz/version)
ATOM_IMG_VERSION=$(shell cat $(REPODIR)/packages/x86/ffritz/version)

ARM_EXT_IMAGE = ffritz-arm-$(ARM_IMG_VERSION)-$(SOC)-fos7$(GLIBC_SUFFIX).tar.gz
APP_IMAGE     = ffritz-app-$(ATOM_IMG_VERSION)-$(SOC)-fos7$(GLIBC_SUFFIX).tar
