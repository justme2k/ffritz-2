#!/bin/sh
#
# Service to forward some sensors to a local mqtt/mosqiutto broker
#
# Parameters:
# FB_USER   FritzBox User having proper access rights to read sensors
# FB_PASSWD Password of this user
# FB_HOST   (optional) IP address of box
# REPORT_INTERVAL Interval in seconds (default 30)
# FB_TOPIC  mqtt topic (default FB_TOPIC=FritzBox
#
# Data is reported as flow to under the topic 
#  $FB_TOPIC/(steckdose|thermostat|rauchmelder)/$name/$value
# with name being the name of the device given by the user in the FB gui.
#
# value is device specific (see new_device function)
#
# Last reading is stored in /tmp/devicelistinfos.xml
#

# replacement for perl version of iconv
#
iconv()
{
  PERL5LIB=/usr/local/lib/perl5/5.34.0 perl /usr/local/bin/piconv $*
}

# Get a new session ID in SID
#
get_sid()
{
  test -z $FB_USER && { echo FB_USER not set 1>&2; return 1; }
  test -z $FB_PASSWD && { echo FB_PASSWD not set 1>&2; return 1; }
  test -z $FB_HOST && FB_HOST=127.0.0.1

  challenge=`curl -s http://$FB_HOST/login_sid.lua | sed -e 's/.*<Challenge>\([^<]*\).*/\1/'`
  token=`echo -n ${challenge}-$FB_PASSWD | iconv -f UTF-8 -t UTF-16LE | md5sum | sed  -e 's/ .*//'`

  resp=`curl -s --data "response=${challenge}-${token}&username=$FB_USER" http://$FB_HOST/login_sid.lua`
  SID=`echo $resp | sed -e 's/.*<SID>\([^<]*\).*/\1/'`

  test -z $SID && { echo curl failed to get SID 1>&2; return 1; }
  test "$SID" = "0000000000000000" && { echo Invalid SID, response: 1>&2; echo $resp 1>&2; return 1; }
  return 0
}

get_val()
{
  name=$1
  shift
  echo $* | sed -e "s/.*$name=\"\([^\"]*\).*/\1/"
}

end_device()
{
  test $device_valid -eq 0 && return

  set $tokens
  while [ "$2" != "" ]; do
        test $verbose -eq 1 && echo $FB_TOPIC/$device_path/$1 $2
	$echo mosquitto_pub -h $FB_HOST -t $FB_TOPIC/$device_path/$1 -m "$2"
	shift
	shift
  done

  device_valid=0
  expected_tokens=""
  tokens=""
  comma=""
}

new_device()
{
  functionbitmask=`get_val functionbitmask $*`
  have_tokens=

  # This takes some functrins identified by functionbitmask into account
  # To support other devices, add the functionbitmask and assign a device_path
  # To add values, add the name to expected_tokens. Make sure the value is a float
  # in the case construct in new_token()
  #
  case $functionbitmask in
    35712) #Steckdose
          device_valid=1;
          device_path=steckdose
          expected_tokens="name present state power celsius"
          ;;
    320) # Thermostat
          device_valid=1;
          device_path=thermostat
          expected_tokens="name present battery celsius windowopenactiv tsoll"
          ;;
    8208) # Rauchmelder
          device_valid=1;
          device_path=rauchmelder
          expected_tokens="name present state"
          ;;
    1048864) # Taster
          device_valid=1;
          device_path=taster
          expected_tokens="name present battery celsius rel_humidity"
          ;;
    *)    device_valid=0;;
  esac
}

new_token()
{
  token=$1
  shift
  line=$*
  
  echo $expected_tokens | grep $token > /dev/null || return
  test "$line" = "<$token>" && return
  echo $have_tokens | grep $token > /dev/null && return

  value=`echo $line | sed -e 's/[^>]*>\([^<]*\)<.*/\1/' -e 's/ [ ]*/_/g'`
  test -z $value && return

  have_tokens="$have_tokens $token"

  # Convert value to a meaningful float value
  #
  nvalue=`echo $value | sed -e 's/-/_/g'`
  case $token in
    power) value=`dc -e "3 k $nvalue 1000 / p"`;;
    celsius) value=`dc -e "1 k $nvalue 10 / p"`;;
    tsoll) value=`dc -e "1 k $nvalue 2 / p"`;;
    *);;
  esac

  if [ $token == name ]; then
    device_path=$device_path/$value
  else
    echo $tokens | grep "$token " >/dev/null || tokens="$tokens $token $value"
    comma=,
  fi
}

## Parse the formatted XML output of getdevicelistinfos and report to mqtt
#
parse_xml()
{
  device_valid=0
  while read line; do
    test "$line" = "" && continue;
    token=`echo $line | sed -e 's/[[:space:]]*<\([^>]*\)>.*/\1/'`
    test -z "$token" && continue;
    set $token
    case $1 in
      device) new_device $token;;
      /device) end_device;;
      /*)     ;;
      *)      test $device_valid -eq 1 && new_token $token $line;;
    esac
  done
}

echo=
verbose=0
while [ ! -z $1 ]; do
  case $1 in
  -h) echo "$0 [-h] [-v] [-p]"; exit 0;;
  -v) verbose=1;;
  -p) echo=echo;;
  esac
  shift
done

get_sid || { echo "Failed to start session" 1>&2; exit 1; }

test -z $FB_TOPIC && FB_TOPIC=FritzBox
test -z $REPORT_INTERVAL && REPORT_INTERVAL=30

while true; do
  curl -s "http://$FB_HOST/webservices/homeautoswitch.lua?switchcmd=getdevicelistinfos&sid=$SID" | xmllint --format -| tee /tmp/devicelistinfos.xml | parse_xml
  sleep $REPORT_INTERVAL
done

