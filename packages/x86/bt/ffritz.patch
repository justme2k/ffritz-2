diff --git a/example/a2dp_sink_demo.c b/example/a2dp_sink_demo.c
index ea2369449..1706f2145 100644
--- a/example/a2dp_sink_demo.c
+++ b/example/a2dp_sink_demo.c
@@ -62,12 +62,16 @@
  */
 // *****************************************************************************
 
+#define _GNU_SOURCE
+#include <fcntl.h>
+#include <unistd.h>
+#include <sys/wait.h>
 #include <inttypes.h>
 #include <stdint.h>
 #include <stdio.h>
 #include <stdlib.h>
 #include <string.h>
-
+#include <signal.h>
 #include "btstack.h"
 #include "btstack_resample.h"
 
@@ -118,10 +122,10 @@ static int       request_frames;
 #define STORE_FROM_PLAYBACK
 
 // WAV File
-#ifdef STORE_TO_WAV_FILE
 static uint32_t audio_frame_count = 0;
 static char * wav_filename = "a2dp_sink_demo.wav";
-#endif
+
+const char *sink_name = "A2DP Sink Demo 00:00:00:00:00:00";
 
 typedef struct {
     int reconfigure;
@@ -148,6 +152,32 @@ static const char * device_addr_string = "5C:F3:70:60:7B:87";
 static bd_addr_t device_addr;
 #endif
 
+// for alsa raw output
+static int use_aplay = 0;
+
+static char s_alsa_dev[32] = "hw:0";
+static char s_sampling_freq[32] = "44100";
+static char s_channels[32] = "2";
+static char *aplay_argv[] =
+{
+  "aplay",
+  "-t",
+  "raw",
+  "-r",
+  s_sampling_freq,
+  "-c",
+  s_channels,
+  "-f",
+  "S16_LE",
+  "-D",
+  s_alsa_dev,
+  "-",
+  NULL
+};
+static char *start_script = NULL;
+static char *volfile = NULL;
+
+
 static btstack_packet_callback_registration_t hci_event_callback_registration;
 
 static uint8_t  sdp_avdtp_sink_service_buffer[150];
@@ -173,6 +203,95 @@ static uint8_t media_sbc_codec_configuration[4];
 static int media_initialized = 0;
 static btstack_resample_t resample_instance;
 
+static pid_t aplay_pid = 0;
+static int pipefd_open = 0;
+static int pipefd[2];
+
+static void media_processing_close(void);
+
+static void sigcld_handler(int signal)
+{
+  int wstatus;
+
+  (void)signal;
+  fprintf(stderr, "SIGCLD received\n");
+  waitpid(aplay_pid, &wstatus, WNOHANG);
+  aplay_pid = 0;
+}
+
+static void aplay_open(void)
+{
+  if (0 != aplay_pid)
+    return;
+    
+  if (pipefd_open)
+  {
+    close(pipefd[0]);
+    close(pipefd[1]);
+    pipefd_open = 0;
+  }
+
+  if (pipe2(pipefd, O_CLOEXEC))
+  {
+    perror("pipe2");
+    exit(1);
+  }
+  pipefd_open = 1;
+
+  if (start_script)
+  {
+    system(start_script);
+  }
+
+  signal(SIGCLD, sigcld_handler);
+  aplay_pid = fork();
+
+  if (-1 == aplay_pid)
+  {
+    perror ("fork");
+    exit(1);
+  }
+
+  if (0 == aplay_pid)
+  {
+    if (dup2(pipefd[0], STDIN_FILENO))
+    {
+      perror("dup2");
+      exit(1);
+    }
+
+    execvp("aplay", aplay_argv);
+  }
+}
+
+static void aplay_close(void)
+{
+  if (aplay_pid)
+  {
+    kill(aplay_pid, SIGKILL);
+  }
+
+  if (pipefd_open)
+  {
+    close(pipefd[0]);
+    close(pipefd[1]);
+
+    pipefd_open = 0;
+  }
+}
+
+static void aplay_output(int num_samples, int16_t * data)
+{
+  if ((0 != aplay_pid) && pipefd_open)
+  {
+    if (-1 == write(pipefd[1], data, num_samples * sizeof(int16_t)))
+    {
+      perror("write");
+      exit(1);
+    }
+  }
+}
+
 /* @section Main Application Setup
  *
  * @text The Listing MainConfiguration shows how to setup AD2P Sink and AVRCP services. 
@@ -275,7 +394,7 @@ static int a2dp_and_avrcp_setup(void){
     // Set local name with a template Bluetooth address, that will be automatically
     // replaced with a actual address once it is available, i.e. when BTstack boots
     // up and starts talking to a Bluetooth module.
-    gap_set_local_name("A2DP Sink Demo 00:00:00:00:00:00");
+    gap_set_local_name(sink_name);
 
     // allot to show up in Bluetooth inquiry
     gap_discoverable_control(1);
@@ -299,9 +418,7 @@ static int a2dp_and_avrcp_setup(void){
     } else {
         printf("Audio playback supported.\n");
     }
-#ifdef STORE_TO_WAV_FILE 
    printf("Audio will be stored to \'%s\' file.\n",  wav_filename);
-#endif
 #endif
     return 0;
 }
@@ -309,10 +426,8 @@ static int a2dp_and_avrcp_setup(void){
 
 static void playback_handler(int16_t * buffer, uint16_t num_audio_frames){
 
-#ifdef STORE_TO_WAV_FILE
     int       wav_samples = num_audio_frames * NUM_CHANNELS;
     int16_t * wav_buffer  = buffer;
-#endif
     
     // called from lower-layer but guaranteed to be on main thread
     if (sbc_frame_size == 0){
@@ -336,9 +451,16 @@ static void playback_handler(int16_t * buffer, uint16_t num_audio_frames){
         btstack_sbc_decoder_process_data(&state, 0, sbc_frame, sbc_frame_size);
     }
 
+    if (use_aplay)
+    {
+      audio_frame_count += num_audio_frames;
+      aplay_output(wav_samples, wav_buffer);
+    }
 #ifdef STORE_TO_WAV_FILE
-    audio_frame_count += num_audio_frames;
-    wav_writer_write_int16(wav_samples, wav_buffer);
+    else {
+      audio_frame_count += num_audio_frames;
+      wav_writer_write_int16(wav_samples, wav_buffer);
+    }
 #endif
 }
 
@@ -349,9 +471,16 @@ static void handle_pcm_data(int16_t * data, int num_audio_frames, int num_channe
 
     const btstack_audio_sink_t * audio_sink = btstack_audio_sink_get_instance();
     if (!audio_sink){
+        if (use_aplay)
+        {
+          audio_frame_count += num_audio_frames;
+          aplay_output (num_audio_frames * NUM_CHANNELS, data);
+        }
 #ifdef STORE_TO_WAV_FILE
-        audio_frame_count += num_audio_frames;
-        wav_writer_write_int16(num_audio_frames * NUM_CHANNELS, data);
+        else {
+          audio_frame_count += num_audio_frames;
+          wav_writer_write_int16(num_audio_frames * NUM_CHANNELS, data);
+        }
 #endif
         return;
     }
@@ -381,8 +510,20 @@ static int media_processing_init(media_codec_configuration_sbc_t configuration){
 
     btstack_sbc_decoder_init(&state, mode, handle_pcm_data, NULL);
 
+    if (!strncmp(wav_filename, "alsa:", strlen("alsa:")))
+    {
+      aplay_close();
+
+      use_aplay = 1;
+      strncpy(s_alsa_dev, wav_filename + strlen("alsa:"), 32);
+      sprintf(s_sampling_freq, "%d", configuration.sampling_frequency);
+      sprintf(s_channels, "%d", configuration.num_channels);
+      aplay_open();
+    }
 #ifdef STORE_TO_WAV_FILE
-    wav_writer_open(wav_filename, configuration.num_channels, configuration.sampling_frequency);
+    else {
+      wav_writer_open(wav_filename, configuration.num_channels, configuration.sampling_frequency);
+    }
 #endif
 
     btstack_ring_buffer_init(&sbc_frame_ring_buffer, sbc_frame_storage, sizeof(sbc_frame_storage));
@@ -404,6 +545,8 @@ static void media_processing_start(void){
     if (!media_initialized) return;
     // setup audio playback
     const btstack_audio_sink_t * audio = btstack_audio_sink_get_instance();
+//    if (use_aplay)
+//        aplay_open();
     if (audio){
         audio->start_stream();
     }
@@ -414,6 +557,8 @@ static void media_processing_pause(void){
     if (!media_initialized) return;
     // stop audio playback
     audio_stream_started = 0;
+//    if (use_aplay)
+//        aplay_close();
     const btstack_audio_sink_t * audio = btstack_audio_sink_get_instance();
     if (audio){
         audio->stop_stream();
@@ -426,12 +571,21 @@ static void media_processing_close(void){
     audio_stream_started = 0;
     sbc_frame_size = 0;
 
-#ifdef STORE_TO_WAV_FILE                 
-    wav_writer_close();
-    uint32_t total_frames_nr = state.good_frames_nr + state.bad_frames_nr + state.zero_frames_nr;
+    if (use_aplay) {
+      aplay_close();
+      uint32_t total_frames_nr = state.good_frames_nr + state.bad_frames_nr + state.zero_frames_nr;
 
-    printf("WAV Writer: Decoding done. Processed %u SBC frames:\n - %d good\n - %d bad\n", total_frames_nr, state.good_frames_nr, total_frames_nr - state.good_frames_nr);
-    printf("WAV Writer: Wrote %u audio frames to wav file: %s\n", audio_frame_count, wav_filename);
+      printf("APLAY Writer: Decoding done. Processed %u SBC frames:\n - %d good\n - %d bad\n", total_frames_nr, state.good_frames_nr, total_frames_nr - state.good_frames_nr);
+      printf("APLAY Writer: Wrote %u audio frames to raw file: %s\n", audio_frame_count, wav_filename);
+    }
+#ifdef STORE_TO_WAV_FILE
+    else {
+      wav_writer_close();
+      uint32_t total_frames_nr = state.good_frames_nr + state.bad_frames_nr + state.zero_frames_nr;
+
+      printf("WAV Writer: Decoding done. Processed %u SBC frames:\n - %d good\n - %d bad\n", total_frames_nr, state.good_frames_nr, total_frames_nr - state.good_frames_nr);
+      printf("WAV Writer: Wrote %u audio frames to wav file: %s\n", audio_frame_count, wav_filename);
+    }
 #endif
 
     // stop audio playback
@@ -725,6 +879,18 @@ static void avrcp_target_packet_handler(uint8_t packet_type, uint16_t channel, u
             volume = avrcp_subevent_notification_volume_changed_get_absolute_volume(packet);
             volume_percentage = volume * 100 / 127;
             printf("AVRCP Target    : Volume set to %d%% (%d)\n", volume_percentage, volume);
+
+            if (volfile != NULL)
+            {
+              FILE *vf = fopen(volfile, "w");
+
+              if (vf)
+              {
+                fprintf (vf, "%d", (int)volume_percentage);
+                fclose(vf);
+              }
+            }
+
             avrcp_volume_changed(volume);
             break;
         
@@ -840,6 +1006,8 @@ static void a2dp_sink_packet_handler(uint8_t packet_type, uint16_t channel, uint
 #endif
         case A2DP_SUBEVENT_STREAM_STARTED:
             printf("A2DP  Sink      : Stream started\n");
+//            if (use_aplay)
+//              aplay_open();
             // audio stream is started when buffer reaches minimal level
             break;
         
@@ -1080,6 +1248,35 @@ int btstack_main(int argc, const char * argv[]);
 int btstack_main(int argc, const char * argv[]){
     UNUSED(argc);
     (void)argv;
+    const char *usage = "usage: %s [options] [-h] -- [-n sink-name] [-f out-file] [-s start-script] [-v volfile]\n";
+
+    int c;
+    while ((c = getopt(argc, (char**)argv, "n:f:s:v:H")) != -1) switch (c)
+    {
+      case 'n':
+        sink_name = optarg;
+        break;
+      case 'f':
+        wav_filename = optarg;
+        break;
+      case 's':
+        start_script = optarg;
+        break;
+      case 'v':
+        volfile = optarg;
+        break;
+      case 'h':
+        printf(usage, argv[0]);
+        exit (0);
+        break;
+      default:
+        fprintf (stderr, usage, argv[0]);
+        exit(1);
+        break;
+    }
+
+    printf ("name: %s  output=%s\n", sink_name, wav_filename);
+ 
 
     a2dp_and_avrcp_setup();
 
diff --git a/platform/libusb/hci_transport_h2_libusb.c b/platform/libusb/hci_transport_h2_libusb.c
index a3ee9696f..6bd786f1b 100644
--- a/platform/libusb/hci_transport_h2_libusb.c
+++ b/platform/libusb/hci_transport_h2_libusb.c
@@ -85,6 +85,14 @@
 
 #define ASYNC_POLLING_INTERVAL_MS 1
 
+#ifndef LIBUSB_LOG_LEVEL_WARNING
+#define LIBUSB_LOG_LEVEL_WARNING 1
+#endif
+
+#ifndef LIBUSB_LOG_LEVEL_ERROR
+#define LIBUSB_LOG_LEVEL_ERROR 2
+#endif
+
 //
 // Bluetooth USB Transport Alternate Settings:
 //
diff --git a/platform/posix/btstack_link_key_db_fs.c b/platform/posix/btstack_link_key_db_fs.c
index e717c42be..2539e8bbc 100644
--- a/platform/posix/btstack_link_key_db_fs.c
+++ b/platform/posix/btstack_link_key_db_fs.c
@@ -54,7 +54,7 @@
 #ifdef _WIN32
 #define LINK_KEY_PATH ""
 #else
-#define LINK_KEY_PATH "/tmp/"
+#define LINK_KEY_PATH "/var/run/bt/"
 #endif
 #endif
 
diff --git a/port/libusb/Makefile b/port/libusb/Makefile
index e257d1798..116badecf 100644
--- a/port/libusb/Makefile
+++ b/port/libusb/Makefile
@@ -1,3 +1,8 @@
+SYSROOT     = $(BUILDROOT)/output/host/usr/i686-buildroot-linux-gnu/sysroot
+TOOLCHAIN   = $(shell cd $(BUILDROOT)/output/host/usr/bin/; pwd)
+export PATH := $(TOOLCHAIN):$(PATH)
+CC=i686-linux-gcc
+
 # Makefile for libusb based examples
 BTSTACK_ROOT ?= ../..
 
@@ -33,8 +38,10 @@ VPATH += ${BTSTACK_ROOT}/chipset/realtek
 VPATH += ${BTSTACK_ROOT}/chipset/zephyr
 
 # use pkg-config
-CFLAGS  += $(shell pkg-config libusb-1.0 --cflags)
-LDFLAGS += $(shell pkg-config libusb-1.0 --libs)
+#CFLAGS  += $(shell pkg-config libusb-1.0 --cflags)
+#LDFLAGS += $(shell pkg-config libusb-1.0 --libs)
+CFLAGS  += -I$(SYSROOT)/usr/include/libusb-1.0
+LDFLAGS += -L$(SYSROOT)/usr/lib -lusb-1.0
 
 # add pthread for ctrl-c signal handler
 LDFLAGS += -lpthread
diff --git a/port/libusb/btstack_config.h b/port/libusb/btstack_config.h
index c91db2bc5..a85b5492e 100644
--- a/port/libusb/btstack_config.h
+++ b/port/libusb/btstack_config.h
@@ -9,7 +9,7 @@
 
 // Port related features
 #define HAVE_ASSERT
-#define HAVE_BTSTACK_STDIN
+#undef HAVE_BTSTACK_STDIN
 #define HAVE_MALLOC
 #define HAVE_POSIX_FILE_IO
 #define HAVE_POSIX_TIME
diff --git a/tool/metrics/btstack_config.h b/tool/metrics/btstack_config.h
index 465fd0654..2b39dd437 100644
--- a/tool/metrics/btstack_config.h
+++ b/tool/metrics/btstack_config.h
@@ -6,7 +6,7 @@
 #define BTSTACK_CONFIG_H
 
 // Port related features
-#define HAVE_BTSTACK_STDIN
+#undef HAVE_BTSTACK_STDIN
 
 // BTstack features that can be enabled
 #define ENABLE_ATT_DELAYED_RESPONSE
